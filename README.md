**Michal and Company Geode Tables**

Geode furniture offers an adventurous opportunity to experiment. Going through different geodes to see which one would best suit a table is a fun and learning experience. There are different geode table designs. They include center, side, cocktail and coffee tables. At Michal& Co we have a large selection of organic home decor pieces that will bring your room to life and can guide you on which geode stones are right for you.

Visit [https://michalandcompany.com/geode-furniture/](https://michalandcompany.com/geode-furniture/)

---
